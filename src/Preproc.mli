open Utils.Defs

(* Read the image file whose path is given and binarize it. *)
val read_image: string -> BinImg.t

(* Crop the image. *)
val crop_image: BinImg.t -> BinImg.t

(* The combination of the two functions above. *)
val preproc_image: string -> BinImg.t

(* Extract a part of the image. *)
val img_sub: BinImg.t -> Rect.t -> BinImg.t

(* Identify and extract the black connex regions of the image. *)
val connex_areas: BinImg.t -> (Rect.t * BinImg.t) list
