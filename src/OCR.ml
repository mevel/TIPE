(* The neural network used for symbol recognition. *)
let net = ref (SigmoidMlPerceptron.random
   Config.d Config.c [|Config.d; Config.c|] 1. 1.)

let save_net filepath =
  Utils.save !net filepath

let restore_net filepath =
  net := Utils.restore filepath

(* Extract the features of the image whose path is given. *)
let get_features filepath =
  filepath
  |> Preproc.preproc_image
  |> Features.extract_features

let get_data dir = (* Returns an array of arrays of tuples (features,target) *)
  Array.mapi
   (fun k label ->
     let t = Array.make Config.c 0. in t.(k) <- 1.;
     let path = dir ^ label ^ "/" in
     path
     |> Sys.readdir
     |> Array.map (fun filename -> let x = get_features (path^filename) in (x,t))
   )
   Config.labels

let train data =
  for i = 1 to 200 do
    NeuralNetwork.learn_random_base (!net) 0.5 data
  done
  
let feed x =
  (!net)#feed x

let recognize img =
  img
  |> Features.extract_features
  |> feed
  |> Array.mapi (fun k yk -> (k,yk))
  |> Array.to_list
  |> List.filter (fun (_,yk) -> Config.class_threshold <= yk)
  |> List.sort (fun (_,yk) (_,yk') -> Pervasives.compare yk' yk)
