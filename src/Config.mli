(* Filesystem paths. *)
val ocr_training_dir: string (* must follow the structure required by “OCR.get_data”  *)
val ocr_testing_dir: string  (* idem *)
val net_file: string
val rules_file: string

(* Print timers? *)
val timers: bool

(* Class labels. *)
val labels: string array
(* Number of classes. *)
val c: int

(* Number of divisions along each side of an image when computing its profile. *)
val profile_div: int
(* Number of features. *)
val d: int

(* Minimal output value for a class to be considered as recognized. *)
val class_threshold: float

(* Minimal red+green+blue sum for a pixel to be considered dark. *)
val dark_threshold: int
