(*
 *  Training program.
 *)

let training () =
  (* Extracting training data features for all classes. *)
  let timer = new Utils.timer in
  let data =
   Config.ocr_training_dir |> OCR.get_data |> Array.to_list |> Array.concat in
  (* Neural network. *)
  timer#step ();
  OCR.train data;
  timer#print ["features"; "training"];
  OCR.save_net Config.net_file

(* Setup the neural network before using it. *)
let restore_or_train () =
  try
    OCR.restore_net Config.net_file
  with _ -> begin
    Printf.printf "Failed to restore the neural network. Training it instead…%!";
    training ();
    Printf.printf " done.\n%!"
  end

(*
 *  Automated testing.
 *)

let best_scored_class y =
  let kmax = ref 0 in
  let max = ref y.(0) in
  for k = 1 to Array.length y - 1 do
    if y.(k) > !max then begin
      kmax := k;
      max := y.(k)
    end
  done;
  !kmax

let testing () =
  restore_or_train ();
  let timer = new Utils.timer in
  let data = OCR.get_data Config.ocr_testing_dir in
  timer#step ();
  let count = ref 0
  and count_wrong = Array.make Config.c 0
  and names_wrong = ref []
  and error = ref 0.0 in
  for k = 0 to Config.c-1 do
    let dirpath = Config.ocr_testing_dir ^ Config.labels.(k) ^ "/" in
    let filenames = Sys.readdir dirpath in
    Array.iteri
     (fun i (x,y0) ->
       let y = OCR.feed x in
       error :=
       y
       |> Array.mapi (fun i y_i -> y_i -. y0.(i))
       |> Array.fold_left (fun err dy -> err +. dy*.dy) !error;
       if best_scored_class y <> k then begin
         count_wrong.(k) <- count_wrong.(k) + 1;
         names_wrong := (dirpath ^ filenames.(i)) :: !names_wrong
       end;
       incr count
     )
     data.(k)
  done;
  timer#print ["features"; "error computation"];
  let total_wrong = Array.fold_left (+) 0 count_wrong in
  Printf.printf "errors: %u/%u samples; detail:\n" total_wrong !count;
  Array.iteri
   (fun k n ->
     Printf.printf "  %s: %u/%u\n" Config.labels.(k) n (Array.length data.(k))
   )
   count_wrong;
  if total_wrong <> 0 then begin
    Printf.printf "misrecognized files:\n";
    List.iter (fun filepath -> Printf.printf "  %s\n" filepath) !names_wrong
  end;
  Printf.printf "mean error = %f\n" (!error /. float !count)

(*
 *  Recognition program.
 *)

let recognition paths =
  restore_or_train ();
  List.iter
   (fun path ->
     Printf.printf "%s\n" path;
     path
     |> Preproc.preproc_image
     |> OCR.recognize
     |> List.iter
      (fun (k,yk) ->
        Printf.printf "  %s: %f\n" Config.labels.(k) yk
      )
   )
   paths

(*
 *  Main program.
 *)

let () =
  match List.tl (Array.to_list Sys.argv) with
  | "--train" :: _  -> training ()
  | "--test" :: _   -> testing ()
  | "--" :: paths
  | paths           -> recognition paths
