open Utils.Defs
open Lexer

(* A rule for transforming the graph. *)
type rule = {
  name: string;             (* a name describing the rule (for printing) *)
  priors: int array;        (* priority of the rule for each node of the subgraph *)
  g: Graph.t;               (* template subgraph *)
  p: (int->Dyn.t) -> term;  (* production rule (called with a function giving
                               the value mapped to the template’s symbolic
                               variable #n) *)
  a: Geom.t array -> Geom.t (* production rule for the attributes *)
}

(* Some facilities to write rules. *)
val get: (int->Dyn.t) -> int -> 'a
val rects_union:      Geom.t array -> Rect.t
val baselines_mean:   Geom.t array -> int
val baseheights_mean: Geom.t array -> int
val baseheights_max:  Geom.t array -> int

(* Given a set of rules whose all nodes have a precedence attributed, return
 * generated rules applicable to a same node, sorted by descending precedence.
 * The treatment of a rule is as follow: for each node #i0, duplicate the rule
 * and exchange nodes #0 and #i0 (so ‘match_subgraph’ will begin with node #i0).
 * The output is of the form (prior,i0,rule) where prior is the precendence
 * attributed to node #i0. *)
val treat_rules: rule list -> (int * int * rule) list

(* Try to match the given template subgraph in the graph, node #0 of the template
 * being mapped to node #i of the graph. If successful, return an array storing
 * the mapping from the node indexes in the template to node indexes in the
 * graph, and a hashtable giving the values of terms in the graph bound to the
 * template’s symbolic variables. *)
val match_subgraph: Graph.t -> Graph.t -> int
  -> (int array * (int,Dyn.t) Hashtbl.t) option

(* Given the result of a successful match (provided by ‘match_subgraph’ above),
 * apply the rule. *)
val subst: rule -> Graph.t -> (int array * (int,Dyn.t) Hashtbl.t) -> unit

(* Try to apply one of the rules given (conforming to the output of ‘treat_rules’
 * above), accounting for their precedence. Return true if successful. *)
val apply_rules: (int * int * rule) array  -> Graph.t -> bool
