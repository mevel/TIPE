open Grammar
open Graph
open Lexer
open Utils.Defs.Geom

let rules =
[
  (* i letter *)
  { name = "i letter";
    priors = [| 2100; 2100 |];
    g = { geoms = [||];
          terms = [| Atom (Val "i"); Atom (Val "dot") |];
          adj =
           [| [| 0; -5 |];
              [| 5;  0 |] |]
        };
    p = (fun sym -> Expr (Val (E_Letter 'i')));
    a = (fun geoms ->
          { rect = rects_union geoms;
            baseline = geoms.(0).baseline;
            baseheight = geoms.(0).baseheight;
          } );
  };
  (* equality sign *)
  { name = "equality sign";
    priors = [| 2100; 2100 |];
    g = { geoms = [||];
          terms = [| Atom (Val "hline"); Atom (Val "hline") |];
          adj =
           [| [| 0; -5 |];
              [| 5;  0 |] |]
        };
    p = (fun sym -> BinOpt (Val "eq"));
    a = (fun geoms ->
          { rect = rects_union geoms;
            baseline = geoms.(0).baseline;
            baseheight = geoms.(0).baseline - geoms.(1).baseline;
          } );
  };
  (* digit concatenation *)
  { name = "digit concatenation";
    priors = [| 2100; 2100 |];
    g = { geoms = [||];
          terms = [| Number (Sym 0); Number (Sym 1) |];
          adj =
           [| [|  0; 3 |];
              [| -3; 0 |] |]
        };
    p = (fun sym -> Number (Val (get sym 0 ^ get sym 1)));
    a = (fun geoms ->
          { rect = rects_union geoms;
            baseline = baselines_mean geoms;
            baseheight = baseheights_mean geoms;
          } );
  };
  (* cast of number lexem to number expression *)
  { name = "cast of number lexem to number expression";
    priors = [| 2000 |];
    g = { geoms = [||];
          terms = [| Number (Sym 0) |];
          adj = [| [|  0 |] |]
        };
    p = (fun sym -> Expr (Val (E_Number (get sym 0))));
    a = (fun geoms -> geoms.(0) );
  };
  (* subscript *)
  { name = "subscript";
    priors = [| 600; 10 |];
    g = { geoms = [||];
          terms = [| Expr (Sym 0); Expr (Sym 1) |];
          adj =
           [| [|  0; 4 |];
              [| -4; 0 |] |]
        };
    p = (fun sym -> Expr (Val (E_Sub (get sym 0, get sym 1))));
    a = (fun geoms ->
          { rect = rects_union geoms;
            baseline = geoms.(0).baseline;
            baseheight = geoms.(0).baseheight;
          } );
  };
  (* superscript *)
  { name = "superscript";
    priors = [| 600; 10 |];
    g = { geoms = [||];
          terms = [| Expr (Sym 0); Expr (Sym 1) |];
          adj =
           [| [|  0; 2 |];
              [| -2; 0 |] |]
        };
    p = (fun sym -> Expr (Val (E_Pow (get sym 0, get sym 1))));
    a = (fun geoms ->
          { rect = rects_union geoms;
            baseline = geoms.(0).baseline;
            baseheight = geoms.(0).baseheight;
          } );
  };
  (* subscript & superscript combined *)
  { name = "subscript & superscript combined";
    priors = [| 610; 10; 10 |];
    g = { geoms = [||];
          terms = [| Expr (Sym 0); Expr (Sym 1); Expr (Sym 2) |];
          adj =
           [| [|  0; 4;  2 |];
              [| -4; 0; -5 |];
              [| -2; 5;  0 |] |]
        };
    p = (fun sym -> Expr (Val (E_Pow (E_Sub (get sym 0, get sym 1), get sym 2))));
    a = (fun geoms ->
          { rect = rects_union geoms;
            baseline = geoms.(0).baseline;
            baseheight = geoms.(0).baseheight;
          } );
  };
  (* implicit product *)
  { name = "implicit product";
    priors = [| 500; 501 |];
    g = { geoms = [||];
          terms = [| Expr (Sym 0); Expr (Sym 1) |];
          adj =
           [| [|  0; 3 |];
              [| -3; 0 |] |]
        };
    p = (fun sym -> Expr (Val (E_Cat (get sym 0, get sym 1))));
    a = (fun geoms ->
          { rect = rects_union geoms;
            baseline = baselines_mean geoms;
            baseheight = baseheights_mean geoms;
          } );
  };
  (* product *)
  { name = "product";
    priors = [| 1400; 400; 401 |];
    g = { geoms = [||];
          terms = [| BinOpt (Val "times"); Expr (Sym 1); Expr (Sym 2) |];
          adj =
           [| [|  0; -3; 3 |];
              [|  3;  0; 0 |];
              [| -3;  0; 0 |] |]
        };
    p = (fun sym -> Expr (Val (E_BinOpt ("times", get sym 1, get sym 2))));
    a = (fun geoms ->
          { rect = rects_union geoms;
            baseline = baselines_mean geoms;
            baseheight = baseheights_max geoms;
          } );
  };
  (* addition *)
  { name = "addition";
    priors = [| 1200; 200; 201 |];
    g = { geoms = [||];
          terms = [| BinOpt (Val "plus"); Expr (Sym 1); Expr (Sym 2) |];
          adj =
           [| [|  0; -3; 3 |];
              [|  3;  0; 0 |];
              [| -3;  0; 0 |] |]
        };
    p = (fun sym -> Expr (Val (E_BinOpt ("plus", get sym 1, get sym 2))));
    a = (fun geoms ->
          { rect = rects_union geoms;
            baseline = baselines_mean geoms;
            baseheight = baseheights_max geoms;
          } );
  };
  (* subtraction *)
  { name = "subtraction";
    priors = [| 1200; 200; 201 |];
    g = { geoms = [||];
          terms = [| Atom (Val "hline"); Expr (Sym 1); Expr (Sym 2) |];
          adj =
           [| [|  0; -3; 3 |];
              [|  3;  0; 0 |];
              [| -3;  0; 0 |] |]
        };
    p = (fun sym -> Expr (Val (E_BinOpt ("minus", get sym 1, get sym 2))));
    a = (fun geoms ->
          { rect = rects_union geoms;
            baseline = baselines_mean geoms;
            baseheight = baseheights_max geoms;
          } );
  };
  (* equality *)
  { name = "equality";
    priors = [| 1000; 100; 101 |];
    g = { geoms = [||];
          terms = [| BinOpt (Val "eq"); Expr (Sym 1); Expr (Sym 2) |];
          adj =
           [| [|  0; -3; 3 |];
              [|  3;  0; 0 |];
              [| -3;  0; 0 |] |]
        };
    p = (fun sym -> Expr (Val (E_BinOpt ("eq", get sym 1, get sym 2))));
    a = (fun geoms ->
          { rect = rects_union geoms;
            baseline = baselines_mean geoms;
            baseheight = baseheights_max geoms;
          } );
  };
  (* “big” operator (sum, product, integral) *)
  { name = "“big” operator (sum, product, integral)";
    priors = [| 1300; 10; 10; 300 |];
    g = { geoms = [||];
          terms = [| BigOpt (Sym 0); Expr (Sym 1); Expr (Sym 2); Expr (Sym 3) |];
          adj =
           [| [|  0; 5; -5; 3 |];
              [| -5; 0;  0; 0 |];
              [|  5; 0;  0; 0 |];
              [| -3; 0;  0; 0 |] |]
        };
    p = (fun sym -> Expr (Val (E_BigOpt (get sym 0, get sym 1, get sym 2, get sym 3))));
    a = (fun geoms ->
          { rect = rects_union geoms;
            baseline = (geoms.(0).baseline + geoms.(3).baseline) / 2;
            baseheight = geoms.(3).baseheight;
          } );
  };
  (* fraction *)
  { name = "fraction";
    priors = [| 1500; 10; 10 |];
    g = { geoms = [||];
          terms = [| Atom (Val "hline"); Expr (Sym 1); Expr (Sym 2) |];
          adj =
           [| [|  0; -5; 5 |];
              [|  5;  0; 0 |];
              [| -5;  0; 0 |] |]
        };
    p = (fun sym -> Expr (Val (E_Fraction (get sym 1, get sym 2))));
    a = (fun geoms ->
          { rect = rects_union geoms;
            baseline = geoms.(0).baseline;
            baseheight = baseheights_max geoms;
          } );
  };
  (* parenthesis *)
  { name = "parenthesis";
    priors = [| 1900; 10; 1500 |];
    g = { geoms = [||];
          terms = [| LParens (Sym 0); Expr (Sym 1); RParens (Sym 2) |];
          adj =
           [| [|  0;  3; 0 |];
              [| -3;  0; 3 |];
              [|  0; -3; 0 |] |]
        };
    p = (fun sym -> Expr (Val (E_Parens (get sym 0, get sym 1))));
    a = (fun geoms ->
          { rect = rects_union geoms;
            baseline = baselines_mean geoms;
            baseheight = geoms.(1).baseheight;
          } );
  };
]
