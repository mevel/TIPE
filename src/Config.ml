let ocr_training_dir = "ocr.d/train/"
let ocr_testing_dir =  "ocr.d/test/"
let net_file =         "ocr.d/net.bin"
let rules_file =       "mathform.d/rules.bin"

let timers = false

(* Class labels are automatically deduced from the training dir and sorted by
 * alphabetic order. *)
let labels =
  let labels = Sys.readdir ocr_training_dir in
  Array.sort Pervasives.compare labels;
  labels

let c = Array.length labels

let profile_div = 8
let d = 2 + 4*profile_div
(* TODO: More evolved selection of which features are being used (currently,
 * we need to change Features.ml and update the field “d” here consequently). *)

let class_threshold = 0.1

let dark_threshold = 300
