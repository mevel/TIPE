open Utils.Defs
open Utils.Defs.Rect

type 'a formal =
  | Sym of int
  | Val of 'a

type expr =
  | E_Number of string
  | E_Letter of char
  | E_Cat of expr * expr
  | E_Sub of expr * expr
  | E_Pow of expr * expr
  | E_Fraction of expr * expr
  | E_BinOpt of string * expr * expr
  | E_BigOpt of string * expr * expr * expr
  | E_Parens of char * expr

type term =
  | Atom of string formal
  | Number of string formal
(*| Letter of char formal*)
  | BinOpt of string formal
  | BigOpt of string formal
  | LParens of char formal
  | RParens of char formal
  | Expr of expr formal

exception Unknown of string

(* Translate the symbol given as an integer code to a string. *)
let get_label = function
  | -1 -> "" (* unknown symbol *)
  | k  -> Config.labels.(k)

(* Compute the geometric attributes (baseline, baseheight) of a symbol. *)
let metrics rect label =
  let (d, h) =
   match label with
   | "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9"
     -> (0, rect.h * 1/2)
   | "a" | "c" | "e" | "i"
     -> (0, rect.h)
   | "b" | "d"
     -> (0, rect.h * 2/5)
   | "f"
     -> (-rect.h/3, rect.h * 1/3)
   | "plus" | "minus" | "times" | "div"
     -> (0, rect.h)
   | "Sigma" | "Pi"
     -> (0, rect.h * 1/3)
   | "Integral"
     -> (-rect.h/4, rect.h * 1/4)
   | "lparenthesis" | "rparenthesis"
     -> (-rect.h/4, rect.h * 1/2)
   | "hline"
   | "dot"
     -> (0, rect.h)
   | "" (* unknown symbol *)
     -> (0, rect.h)
   | _
     -> raise (Unknown label)
  in
  (rect.y + rect.h + d, h)

let lexical_analysis (rect,k) =
  let label = get_label k in
  let lex =
    if String.length label = 1 then
      match label.[0] with
      | '0'..'9'
        -> Number (Val label)
      | 'i' | 'j' -> Atom (Val label)
      | 'a'..'z'
      | 'A'..'Z' as c
        -> Expr (Val (E_Letter c))
      | _ -> raise (Unknown label)
    else
      match label with
      | "plus"
      | "minus"
      | "times"
      | "div"
      | "lt"
      | "lteq"
      | "gt"
      | "gteq"
      | "eq"
      | "neq"
        -> BinOpt (Val label)
      | "Sigma"
      | "Pi"
      | "Integral"
        -> BigOpt (Val label)
      | "lparenthesis" -> LParens (Val '(')
      | "rparenthesis" -> RParens (Val ')')
    (*| "i" | "j"*)
      | "hline"
      | "dot"
      | ""
        -> Atom (Val label)
      | _ -> raise (Unknown label)
  in
  let (baseline,baseheight) = metrics rect label in
  (lex, Geom.({rect; baseline; baseheight}))

let rec expr_to_string = function
  | E_Number s            -> s
  | E_Letter c            -> Printf.sprintf "%c" c
  | E_Cat (e1,e2)         -> Printf.sprintf "(%s) (%s)"
                              (expr_to_string e1) (expr_to_string e2)
  | E_Sub (e1,e2)         -> Printf.sprintf "_ (%s) (%s)"
                              (expr_to_string e1) (expr_to_string e2)
  | E_Pow (e1,e2)         -> Printf.sprintf "^ (%s) (%s)"
                              (expr_to_string e1) (expr_to_string e2)
  | E_Fraction (e1,e2)    -> Printf.sprintf "/ (%s) (%s)"
                              (expr_to_string e1) (expr_to_string e2)
  | E_BinOpt (s,e1,e2)    -> Printf.sprintf "<%s> (%s) (%s)" s
                              (expr_to_string e1) (expr_to_string e2)
  | E_BigOpt (s,e1,e2,e3) -> Printf.sprintf "<%s> (%s) (%s) (%s)" s
                              (expr_to_string e1) (expr_to_string e2)
                              (expr_to_string e3)
  | E_Parens (c,e)        -> Printf.sprintf "'%c' (%s)" c (expr_to_string e)

let to_string = function
  | Atom (Val s)
  | BinOpt (Val s)
  | BigOpt (Val s)  -> Printf.sprintf "<%s>" s
  | LParens (Val c)
  | RParens (Val c) -> Printf.sprintf "'%c'" c
  | Number (Val s)  -> s
(*| Letter (Val c)  -> Printf.sprintf "%c" c*)
  | Expr (Val e)    -> Printf.sprintf "Expr %s" (expr_to_string e)
  | _ -> failwith "Lexer.to_string"
