open Utils.Defs.BinImg

let (//) a b = float a /. float b

(*
 *  Draw image’s outline.
 *)

let get_outline img =
  let img' = {img with px = Utils.copy_matrix img.px} in
  for i = 1 to img.h-2 do
    for j = 1 to img.w-2 do
      if not img.px.(i).(j) && not img.px.(i-1).(j) && not img.px.(i+1).(j)
      && not img.px.(i).(j-1) && not img.px.(i).(j+1) then
        img'.px.(i).(j) <- true
    done
  done;
  img'

(*
 *  Compute image’s “profile”, that is, a small vector representing distances
 *  from each edge to the first black pixel, averaged on some divisions of
 *  the image.
 *)

let get_profile img =
  let search_left i =
    let j = ref 0 in
    while !j < img.w && img.px.(i).(!j) do
      incr j
    done;
    !j
  in
  let search_right i =
    let j = ref (img.w-1) in
    while !j >= 0 && img.px.(i).(!j) do
      decr j
    done;
    img.w-1 - !j
  in
  let search_top j =
    let i = ref 0 in
    while !i < img.h && img.px.(!i).(j) do
      incr i
    done;
    !i
  in
  let search_bottom j =
    let i = ref (img.h-1) in
    while !i >= 0 && img.px.(!i).(j) do
      decr i
    done;
    img.h-1 - !i
  in
  let left = Array.make Config.profile_div 0.0 in
  let right = Array.copy left
  and top = Array.copy left
  and bottom = Array.copy left in
  for i = 0 to img.h-1 do
    let i' = i * Config.profile_div / img.h in
    left.(i') <- left.(i') +. float (search_left i);
    right.(i') <- right.(i') +. float (search_right i)
  done;
  for j = 0 to img.w-1 do
    let j' = j * Config.profile_div / img.w in
    top.(j') <- top.(j') +. float (search_top j);
    bottom.(j') <- bottom.(j') +. float (search_bottom j)
  done;
  let profile = Array.concat [left; right; top; bottom] in
  let factor = float (img.w * img.h) /. float Config.profile_div in
  Array.map (fun x -> x /. factor) profile

(*
 *  Count white holes in the image (use mergefind).
 *)

let count_holes img =
    let px = Array.make_matrix img.h img.w 0
    and partition = new Utils.mergefind in
    partition#add_singleton ();
    for i = 0 to img.h-1 do
      for j = 0 to img.w-1 do
        if img.px.(i).(j) then begin
          let left = if j > 0 then px.(i).(j-1) else 0
          and top = if i > 0 then px.(i-1).(j) else 0 in
          if left >= 0 && top >= 0 then begin
            partition#merge left top;
            px.(i).(j) <- left
          end else if left >= 0 then
            px.(i).(j) <- left
          else if top >= 0 then
            px.(i).(j) <- top
          else begin
            px.(i).(j) <- partition#nmax;
            partition#add_singleton ()
          end
        end else
          px.(i).(j) <- -1
      done;
    done;
    for i = 0 to img.h-1 do
      let left = px.(i).(img.w-1) in
      if left >= 0 then
        partition#merge left 0
    done;
    for j = 0 to img.w-1 do
      let top = px.(img.h-1).(j) in
      if top >= 0 then
        partition#merge top 0
    done;
    partition#count - 1

(*
 *  Count image’s dark pixels.
 *)

let count_dark img =
  let count = ref 0 in
  for i = 0 to img.h-1 do
    for j = 0 to img.w-1 do
      if not img.px.(i).(j) then incr count
    done
  done;
  !count

(*
 *  Features extraction.
 *)

(* NOTE: If changing the number of features, do not forget to update Config.ml. *)

let extract_features img =
  let ratio = img.w // img.h in
  let timer = new Utils.timer in
  (*let dark_prop = count_dark img // (img.w * img.h) in
  timer#step ();*)
  (*let outline = get_outline img in
  timer#step ();
  let outline_len = count_dark outline // (min img.w img.h) in
  timer#step ();*)
  let holes = float (count_holes img) in
  timer#step ();
  let profile = get_profile img in
  timer#stop ();
 if Config.timers then
   timer#print [(*"dark_prop  ";*) (*"outline    "; "outline_len";*) "holes      "; "profile    "];
  Array.append [|ratio; (*dark_prop;*) (*outline_len;*) holes|] profile
