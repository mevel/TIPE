module Defs = struct
  
  module BinImg = struct
    type t = {
      w: int;
      h: int;
      px: bool array array
    }
    let create h w =
      {h; w; px = Array.make_matrix h w true}
  end
  
  module Rect = struct
    type t = {
      mutable y: int;
      mutable x: int;
      mutable h: int;
      mutable w: int
    }
    let to_string {y; x; h; w} =
      Printf.sprintf "(%u,%u)..(%u,%u)" y x (y+h) (x+w)
  end
  
  module Geom = struct
    type t = {
      rect: Rect.t;
      mutable baseline: int;
      mutable baseheight: int;
    }
    let to_string {rect; baseline; baseheight} =
      Printf.sprintf "%s bl=%u, bh=%u" (Rect.to_string rect) baseline baseheight
  end
  
  module Dyn = struct
    type t = Dyn: 'a -> t
    let get (d:t) = (Obj.magic d).(0)
  end
end


exception Break


exception Wont_happen


let copy_matrix mat =
  Array.init (Array.length mat) (fun i -> Array.copy mat.(i))

let swap t i j =
  let tmp = t.(i) in
  t.(i) <- t.(j);
  t.(j) <- tmp

let save x filepath =
  let file = open_out filepath in
  Marshal.to_channel file x [Marshal.Closures];
  close_out file

let restore filepath =
  let file = open_in filepath in
  let x = Marshal.from_channel file in
  close_in file;
  x


class mergefind = object (self)
  
  val mutable forest : int array = [||]
  val mutable nmax = 0
  val mutable count = 0
  
  method nmax = nmax
  method count = count
  
  method add_singleton () =
    forest <- Array.append forest [| nmax |];
    nmax <- nmax+1;
    count <- count+1
  
  method find n =
    let p = forest.(n) in
    if n = p then
      n
    else begin
      let q = self#find p in
      forest.(n) <- q;
      q
    end
  
  method merge m n =
    if m <> n then begin (* Not strictly needed, but appears to be faster. *)
      let m = self#find m
      and n = self#find n in
      if m <> n then begin
        forest.(m) <- n;
        count <- count-1
      end
    end
end


class timer = object (self)
  
  val q = Queue.create ()
  val mutable running = false
  
  initializer 
    self#start ()
  
  method step () =
    Queue.add (Sys.time ()) q
  
  method start () =
    if not running then begin
      Queue.clear q;
      self#step ();
      running <- true
    end
  
  method stop () =
    if running then begin
      self#step ();
      running <- false
    end
  
  method print labels =
    self#stop ();
    if List.length labels <> Queue.length q - 1 then
      failwith "Utils.timer#print: wrong number of labels.";
    let t = ref (Queue.pop q) in
    List.iter
     (fun label ->
       let t' = Queue.pop q in
       Printf.fprintf stderr "%s: %fs\n" label (t' -. !t);
       t := t'
     )
     labels
end
