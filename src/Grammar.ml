open Utils.Defs
open Utils.Defs.Geom
open Utils.Defs.Rect
open Graph
open Lexer

type rule = {
  name: string;
  priors: int array;
  g: Graph.t;
  p: (int->Dyn.t) -> term;
  a: Geom.t array -> Geom.t
}

let get sym i = Dyn.get (sym i)

let rects_union geoms =
  let (y0,x0,y1,x1) =
   Array.fold_left
    (fun (y0,x0,y1,x1) geom ->
      let r = geom.rect in
      let y0 = min y0 r.y
      and x0 = min x0 r.x
      and y1 = max y1 (r.y+r.h)
      and x1 = max x1 (r.x+r.w) in
      (y0,x0,y1,x1)
    )
    (max_int,max_int,0,0)
    geoms
  in
  {y = y0; x = x0; h = y1-y0; w = x1-x0}

let baselines_mean geoms =
  let sum = Array.fold_left (fun sum geom -> sum + geom.baseline) 0 geoms in
  sum / Array.length geoms

let baseheights_mean geoms =
  let sum = Array.fold_left (fun sum geom -> sum + geom.baseheight) 0 geoms in
  sum / Array.length geoms

let baseheights_max geoms =
  Array.fold_left (fun m geom -> max m geom.baseheight) 0 geoms

(* Swap nodes i and j in the rule template. *)
let swap_rule_nodes rule i j =
  let aux t = Utils.swap t i j in
  aux rule.priors;
  aux rule.g.terms;
  aux rule.g.adj;
  Array.iter aux rule.g.adj

let treat_rules rules =
  rules
  |> List.map
   (fun rule ->
      rule.priors
      |> Array.mapi
       (fun i prior ->
         let rule' =
          { name = rule.name;
            priors = Array.copy rule.priors;
            g = Graph.copy rule.g;
            p = rule.p;
            a = rule.a }
         in
         swap_rule_nodes rule' 0 i;
         (prior, i, rule')
       )
      |> Array.to_list
   )
  |> List.flatten
  |> List.sort (fun (prior,_,_) (prior',_,_) -> prior' - prior)

(* Match a value against a possibly symbolic value. If succesful, returns the
 * list of bindings in the form (i,x) where i is the identifier of a symbol
 * and x is the value bound to it. *)
let match_symbolic v0 v =
  match v with
  | Sym _ -> raise Utils.Wont_happen
  | Val x ->
  match v0 with
  | Sym s -> Some [(s, Dyn.Dyn x)]
  | Val x0 ->
    if x0 = x then Some []
    else           None

(* Match a term against a template term. *)
let match_term t0 t =
  match t0 with
  | Atom v0    -> (match t with Atom v    -> match_symbolic v0 v | _ -> None)
  | Number v0  -> (match t with Number v  -> match_symbolic v0 v | _ -> None)
  | BinOpt v0  -> (match t with BinOpt v  -> match_symbolic v0 v | _ -> None)
  | BigOpt v0  -> (match t with BigOpt v  -> match_symbolic v0 v | _ -> None)
  | LParens v0 -> (match t with LParens v -> match_symbolic v0 v | _ -> None)
  | RParens v0 -> (match t with RParens v -> match_symbolic v0 v | _ -> None)
  | Expr v0    -> (match t with Expr v    -> match_symbolic v0 v | _ -> None)

let match_subgraph g0 g i =
  let n0 = Array.length g0.terms
  and n = Array.length g.terms in
  let map = Array.create n0 (-1)
  and used = Array.create n false in
  (* This function tries to match node #i0 of the template with node #i of the
   * graph, then iterate recursively on nodes #j0 of the template for j0 > i0.
   * Return value similar to the ones of ‘match_symbolic’ and ‘match_term’ above. *)
  let rec aux i0 i =
    (* Check that node values match. *)
    match match_term g0.terms.(i0) g.terms.(i) with
    | None -> None
    | Some symbols ->
      begin
        map.(i0) <- i;
        (* Check that edges with already mapped nodes match. *)
        let ok = ref true
        and j0 = ref 0 in
        while !ok && !j0 < i0 do
          ok := ( g0.adj.(i0).(!j0) = g.adj.(i).(map.(!j0)) );
          incr j0
        done;
        if not !ok then
          None
        else if i0 = n0 - 1 then
          Some symbols
        else begin
          used.(i) <- true;
          (* Search for the next node to be mapped. *)
          let found = ref None
          and j = ref 0 in
          while !found = None && !j < n do
            if not used.(!j) then
              found := aux (i0+1) !j;
            incr j
          done;
          used.(i) <- false; 
          match !found with
          | None -> None
          | Some more_symbols -> Some (symbols @ more_symbols)
        end
      end
  in
  if n0 > n then
    None
  else
    match aux 0 i with
    | None -> None
    | Some symbols ->
      let sym_map = Hashtbl.create n0 in
      List.iter (fun (s,v) -> Hashtbl.add sym_map s v) symbols;
      Some (map, sym_map)

let subst_term symbols p =
  p (fun i -> Hashtbl.find symbols i)

let subst rule g (map,symbols) =
  let geoms = Array.map (fun i -> g.geoms.(i)) map in
  let term' = subst_term symbols rule.p
  and geom' = rule.a geoms in
  Array.sort (-) map;
  for i0 = Array.length map - 1 downto 1 do
    Graph.remove_node g map.(i0)
  done;
  Graph.replace_node g map.(0) (term',geom')

let apply_rules rules g =
  let fst (x,_,_) = x
  in
  let n = Array.length g.terms
  and nrules = Array.length rules in
  let k = Array.create n (-1) (* for each node, index of the last rule tried *)
  and results = Array.create n None (* results obtained for those tries *)
  in
  (* Search for the first matching rule at node #i with a precedence higher than
   * min_prior, taking profit of matches already computed. *)
  let search_first_matching_rule i min_prior =
    while results.(i) = None && k.(i) < nrules-1 && fst rules.(k.(i)+1) > min_prior do
      k.(i) <- k.(i) + 1;
      let (_,_,rule) = rules.(k.(i)) in
      results.(i) <- match_subgraph rule.g g i;
    done
  in
  (* Check that no other rule apply with a higher precedence at any node of
   * the matching subgraph. *)
  let is_allowed map priors =
    try
    for i0 = 1 to Array.length map - 1 do
      let i = map.(i0)
      and min_prior = priors.(i0) in
      search_first_matching_rule i min_prior;
      if results.(i) <> None && fst rules.(k.(i)) > min_prior then begin
        let (_,i0,rule) = rules.(k.(i)) in
        Printf.printf "forbidden by rule “%s” (#%u -> #%u)\n" rule.name i0 i;
        raise Utils.Break
      end
    done;
    true
    with Utils.Break -> false
  in
  (* Search for a rule which matches some part of the graph, and such that no
   * rule with a higher precedence apply to a node of the matching subgraph. *)
  try
  for k0 = 0 to nrules-1 do
    let (_,i0,rule) = rules.(k0) in
    for i = 0 to n-1 do
      if k.(i) < k0 then begin
        k.(i) <- k0;
        results.(i) <- match_subgraph rule.g g i
      end;
      if k.(i) = k0 then begin
        match results.(i) with
        | None -> ()
        | Some (map,symbols) ->
          begin
            Printf.printf "found rule “%s” (#%u -> #%u)… " rule.name i0 i;
            if is_allowed map rule.priors then begin
              Utils.swap map 0 i0; (* The function rule.a does not know about the permutation made on nodes when treating the rules, so we need to reverse it. *)
              subst rule g (map,symbols);
              Printf.printf "resulting graph:\n";
              Graph.print g;
              raise Utils.Break
            end
          end
      end
    done
  done;
  false (* The graph could not be simplified anymore. *)
  with Utils.Break -> true
