open Utils.Defs

(* Type used to abstract a value in a term; this allows the writing of template
 * terms including symbolic values which are to be matched in a comparison. *)
type 'a formal =
  | Sym of int (* symbolic value (the integer is an identifier) *)
  | Val of 'a  (* immediate value *)

(* A valid expression. *)
type expr =
  | E_Number of string (* Store numbers as strings since we want exact transcription (e.g. with possible leading zeros). *)
  | E_Letter of char
  | E_Cat of expr * expr
  | E_Sub of expr * expr
  | E_Pow of expr * expr
  | E_Fraction of expr * expr
  | E_BinOpt of string * expr * expr
  | E_BigOpt of string * expr * expr * expr
  | E_Parens of char * expr

(* A “term”, that is, either a symbol or an expression which labels a node
 * in the geometric graph (module “Graph”). *)
type term =
  | Atom of string formal
  | Number of string formal
(*| Letter of char formal*)
  | BinOpt of string formal
  | BigOpt of string formal
  | LParens of char formal
  | RParens of char formal
  | Expr of expr formal

(* Raised when encountering an unknown symbol label. *)
exception Unknown of string

(* Transform a recognized symbol with its position to a node of the graph. *)
val lexical_analysis: (Rect.t * int) -> (term * Geom.t)

(* Convert a term to a string. *)
val to_string: term -> string
