(* Save the current state of the neural network into a file whose path is given. *)
val save_net: string -> unit

(* Restore the state of the network from the file whose path is given. *)
val restore_net: string -> unit

(* Extract features from an image base whose path is given. The image base is a
 * directory containing only subdirectories. Each of them represents a distinct
 * class; its name is the name of that class, and it only contains images
 * belonging to that class.
 * The return value is an array where each element correspond to a class, and
 * is itself the array of data drawn from each image of that class. These
 * data are couples (input,target). *)
val get_data: string -> (float array * float array) array array

(* Train the neural network with the data provided as couples (input,target). *)
val train: (float array * float array) array -> unit

(* Compute the output vector of the neural network for the input vector given. *)
val feed: float array -> float array

(* Recognize an image; returns the most probable classes with their probabilities,
 * sorted by descending order. *)
val recognize: Utils.Defs.BinImg.t -> (int * float) list
