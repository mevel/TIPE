(*
 *  Common type definitions.
 *)

module Defs : sig
  
  (* A black&white image bitmap. *)
  module BinImg : sig
    type t = {
      w: int;
      h: int;
      px: bool array array
    }
    val create: int -> int -> t
  end
  
  (* A rectangle (stores a size and a position). *)
  module Rect : sig
    type t = {
      mutable y: int;
      mutable x: int;
      mutable h: int;
      mutable w: int
    }
    val to_string: t -> string
  end
  
  (* A structure containing informations about a symbol. *)
  module Geom : sig
    type t = {
      rect: Rect.t;
      mutable baseline: int;
      mutable baseheight: int;
    }
    val to_string: t -> string
  end
  
  (* Facility to hide a type to the type system.
   * NO CHECKING, USE ONLY IF THE RESULT TYPE IS KNOWN WITH CERTAINTY. *)
  module Dyn : sig
    type t = Dyn: 'a -> t
    val get: t -> 'a
  end
end

(*
 *  Useful stuff.
 *)

exception Break

exception Wont_happen

val copy_matrix: 'a array array -> 'a array array

val swap: 'a array -> int -> int -> unit

(* Save something into a file whose path is given. *)
val save: 'a -> string -> unit

(* Restore something from the file whose path is given. *)
val restore: string -> 'a

(* 
 *  A merge-find structure.
 *)

class mergefind: object
  method nmax: int
  method count: int
  method add_singleton: unit -> unit
  method merge: int -> int -> unit
  method find: int -> int
end

(*
 *  A timer.
 *)

class timer : object
  method start: unit -> unit
  method step: unit -> unit
  method stop: unit -> unit
  method print: string list -> unit
end
