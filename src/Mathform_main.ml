(* Train the neural network for symbol recognition. *)
let training () =
  Config.ocr_training_dir
  |> OCR.get_data
  |> Array.to_list |> Array.concat
  |> OCR.train;
  OCR.save_net Config.net_file

(* Setup the neural network before using it. *)
let load_network () =
  try
    OCR.restore_net Config.net_file
  with _ -> begin
    Printf.printf "Failed to restore the neural network. Training it instead…%!";
    training ();
    Printf.printf " done.\n%!"
  end

(* The grammar rules treated with ‘Grammar.treat_rules’. *)
let rules = ref [||]

(* Init the grammar rules (we backup and restore them since the preprocessing
 * can be expensive). *)
let load_rules () =
  try
    rules := Utils.restore Config.rules_file
  with _ -> begin
    Printf.printf "Failed to restore treated grammar rules. Generating them instead…%!";
    rules := Array.of_list (Grammar.treat_rules GrammarRules.rules);
    Utils.save !rules Config.rules_file;
    Printf.printf " done.\n%!"
  end

(* Give the most probable class of a symbol (integer code -1 if the recognition
 * failed). *)
let recognize img =
  match OCR.recognize img with
  | (k,_) :: _  -> k
  | []          -> -1 (* unrecognized symbol *)

let testing paths =
  List.iter
   (fun path ->
     Printf.printf "%s\n" path;
     let graph =
      path
      |> Preproc.read_image
      |> Preproc.connex_areas
      |> List.map (fun (rect,img') -> (rect, recognize img'))
      |> List.map Lexer.lexical_analysis
      |> Graph.build
     in
     Graph.print graph;
     begin try
       Graph.only_close graph
     with
     | Graph.Inconsistent (i,j,k) -> Printf.printf "INCONSISTENCY: %u↔%u↔%u\n" i j k
     end;
     Printf.printf "simplified graph:\n";
     Graph.print graph;
     while Array.length graph.Graph.terms > 1 && Grammar.apply_rules !rules graph do
       ()
     done
   )
   paths

let () =
  load_network ();
  load_rules ();
  match List.tl (Array.to_list Sys.argv) with
  | paths  -> testing paths
