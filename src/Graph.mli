open Utils.Defs

(* An edge of graph:
 * ┌───┬───┬───┐  0 = no edge
 * │-4 │-5 │+2 │
 * ├───┼───┼───┤
 * │-3 │±1 │+3 │  +1 = contains, -1 = is contained
 * ├───┼───┼───┤
 * │-2 │+5 │+4 │
 * └───┴───┴───┘
 *)
type edge = int

(* A geometric graph: nodes are couples (term,geom), directed edges indicate
 * the kind of spatial relation which exists between those nodes. *)
type t = {
  mutable terms: Lexer.term array;
  mutable geoms: Geom.t array;
  mutable adj:   edge array array; (* adjacency matrix *)
}

(* Raised when three edges conflict. *)
exception Inconsistent of int*int*int

(* Construct the graph with the nodes given, computing all edges between them. *)
val build: (Lexer.term * Geom.t) list -> t

(* Remove edges between non-close nodes. *)
val only_close: t -> unit

(* Remove node #i from the graph. *)
val remove_node: t -> int -> unit

(* replace node #i by another one, updating edges. *)
val replace_node: t -> int -> (Lexer.term * Geom.t) -> unit

(* Duplicate the graph in memory. *)
val copy: t -> t

(* Print the graph given on stdout. *)
val print: t -> unit
