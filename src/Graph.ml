open Utils.Defs
open Utils.Defs.Geom
open Utils.Defs.Rect

type edge = int

type t = {
  mutable terms: Lexer.term array;
  mutable geoms: Geom.t array;
  mutable adj:   edge array array;
}

exception Inconsistent of int*int*int

let edge_to_string = function
  |  0 -> " "
  |  1 -> "o"
  | -1 -> "⋅"
  |  2 -> "↗"
  | -2 -> "↙"
  |  3 -> "→"
  | -3 -> "←"
  |  4 -> "↘"
  | -4 -> "↖"
  |  5 -> "↓"
  | -5 -> "↑"
  | _ -> raise Utils.Wont_happen

let print g =
  let n = Array.length g.adj in
  for k = 0 to n-1 do
    Printf.printf "%2u: %s\t-> %s\n" k (Geom.to_string g.geoms.(k))
     (Lexer.to_string g.terms.(k))
  done;
  Printf.printf "\n      ";
  for i = 0 to n-1 do
    Printf.printf " %u" (i mod 10)
  done;
  Printf.printf "\n";
  for i = 0 to n-1 do
    Printf.printf "  %2u [" i;
    for j = 0 to n-1 do
      Printf.printf " %s" (edge_to_string g.adj.(i).(j))
    done;
    Printf.printf " ]\n"
  done

(* Exchange nodes #i and #j in the graph. *)
let swap_nodes graph i j =
  let aux t = Utils.swap t i j in
  aux graph.geoms;
  aux graph.terms;
  aux graph.adj;
  Array.iter aux graph.adj

(* Sort nodes from left to right (bubble sort). *)
let reord graph =
  let n = Array.length graph.adj in
  let continue = ref true in
  while !continue do
    continue := false;
    try
    for i = 0 to n-1 do
      for j = 0 to i-1 do
        let edge = graph.adj.(i).(j) in
        if abs edge <> 5 && edge > 0 then begin
          swap_nodes graph i j;
          continue := true;
          raise Utils.Break
        end
      done
    done
    with Utils.Break -> ()
  done

(* Split the plane in 9 regions with respect to the node’s geometry. *)
let split_around_node geom =
  let y0 = geom.baseline - geom.rect.h / 2
  and y1 = geom.baseline
  and x0 = geom.rect.x
  and x1 = geom.rect.x + geom.rect.w in
  (y0,y1,x0,x1)

(* Position of the node with respect to the 9 regions around a reference node
 * (gives the edge between those nodes). *)
let node_relative_position geom0 geom =
  let (y0,y1,x0,x1) = split_around_node geom0 in
  let y2 = geom.baseline
  and x2 = geom.rect.x + geom.rect.w / 2 in
  if x2 <= x0 then
    if y2 < y0       then -4
    else if y2 <= y1 then -3
    else                  -2
  else if x2 < x1 then
    if y2 < y0       then -5
    else if y2 <= y1 then  1
    else                   5
  else
    if y2 < y0       then  2
    else if y2 <= y1 then  3
    else                   4

(* Compute the edge from node #i to node #j of the graph. *)
let build_edge g i j =
  let edge_ij = node_relative_position g.geoms.(i) g.geoms.(j)
  and edge_ji = node_relative_position g.geoms.(j) g.geoms.(i) in
  let edge =
   if edge_ij = -edge_ji then
     edge_ij
   else if edge_ij = 1 then
     1
   else if edge_ji = 1 then
     -1
   else match edge_ij with
   |  3 -> (match edge_ji with -2|(-3)|(-4)  ->  3 | _ -> 0)
   |  5 -> (match edge_ji with -4|(-5)|  2   ->  5 | _ -> 0)
   | -3 -> (match edge_ji with  2|  3 |  4   -> -3 | _ -> 0)
   | -5 -> (match edge_ji with  4|  5 |(-2)  -> -5 | _ -> 0)
   | _ ->
   match edge_ji with
   |  3 -> (match edge_ij with -2|(-4)  -> -3 | _ -> 0)
   |  5 -> (match edge_ij with -4|  2   -> -5 | _ -> 0)
   | -3 -> (match edge_ij with  2|  4   ->  3 | _ -> 0)
   | -5 -> (match edge_ij with  4|(-2)  ->  5 | _ -> 0)
   | _ -> 0
  in
  if edge = 0 then
    Printf.printf "INCONSISTENCY: %u%s%u, %u%s%u\n"
     i (edge_to_string edge_ij) j  j (edge_to_string edge_ji) i;
  g.adj.(i).(j) <- edge;
  g.adj.(j).(i) <- -edge

let build nodes =
  let n = List.length nodes in
  let terms = Array.of_list (List.map fst nodes)
  and geoms = Array.of_list (List.map snd nodes) in
  let adj = Array.create_matrix n n 0 in
  let g = {terms; geoms; adj} in
  for i = 0 to n-1 do
    for j = 0 to i-1 do
      build_edge g i j
    done
  done;
  reord g;
  g

let only_close g =
  (* Remove an edge when another edge exists towards the same direction with a
   * closer node. *)
  let forbidden_seqs = function
  |  0 -> []
  |  1 -> [(1,1)]
  | -1 -> [(-1,-1)]
  |  2 -> [(-5,-5); (-5,2); (-5,3); (2,-5); (2,2); (2,3); (3,-5); (3,2); (3,3)]
  |  3 -> [(3,3); (2,4); (4,2); (3,2); (3,4)]
  |  4 -> [(5,5); (5,4); (5,3); (4,5); (4,4); (4,3); (3,5); (3,4); (3,3)]
  |  5 -> [(5,5)(*; (4,-2); (-2,4)*)]
  | -2 -> [(5,5); (5,-2); (5,-3); (-2,5); (-2,-2); (-2,-3); (-3,5); (-3,-2); (-3,-3)]
  | -3 -> [(-3,-3); (-2,-4); (-4,-2); (-3,-2); (-3,-4)]
  | -4 -> [(-5,-5); (-5,-4); (-5,-3); (-4,-5); (-4,-4); (-4,-3); (-3,-5); (-3,-4); (-3,-3)]
  | -5 -> [(-5,-5)(*; (-4,2); (2,-4)*)]
  | _ -> raise Utils.Wont_happen
  in
  let adj0 = Utils.copy_matrix g.adj in
  let n = Array.length g.terms in
  for i = 0 to n-1 do
    for j = 0 to n-1 do
      let forbidden = forbidden_seqs g.adj.(i).(j) in
      try
      for k = 0 to n-1 do
        if List.mem (adj0.(i).(k), adj0.(k).(j)) forbidden then begin
          g.adj.(i).(j) <- 0;
          g.adj.(j).(i) <- 0;
          raise Utils.Break
        end
      done
      with Utils.Break -> ()
    done
  done;
  (* If it remains more than one edge of the same type, remove all those edges. *)
  let adj0 = Utils.copy_matrix g.adj in
  for i = 0 to n-1 do
    let counts = Array.create 11 0 in
    for j = 0 to n-1 do
      counts.(5+adj0.(i).(j)) <- 1 + counts.(5+adj0.(i).(j))
    done;
    for edge = -5 to 5 do
      if edge <> 0 && counts.(5+edge) > 1 then begin
        for j = 0 to n-1 do
          if adj0.(i).(j) = edge then begin
            g.adj.(i).(j) <- 0;
            g.adj.(j).(i) <- 0
          end
        done
      end
    done
  done

let remove_node graph i =
  let n = Array.length graph.terms in
  let aux t =
    for i = i+1 to n-1 do
      t.(i-1) <- t.(i)
    done;
    Array.sub t 0 (n-1)
  in
  graph.terms <- aux graph.terms;
  graph.geoms <- aux graph.geoms;
  graph.adj <- aux graph.adj;
  graph.adj <- Array.map aux graph.adj

let replace_node g i (term,geom) =
  g.terms.(i) <- term;
  g.geoms.(i) <- geom;
  (*for j = 0 to Array.length g.terms - 1 do
    if j <> i then
      build_edge g i j
  done;*)
  for i = 0 to Array.length g.terms - 1 do
    for j = 0 to Array.length g.terms - 1 do
      if i <> j then
        build_edge g i j
    done
  done;
  only_close g

let copy g =
  { terms = Array.copy g.terms;
    geoms = Array.copy g.geoms;
    adj = Utils.copy_matrix g.adj }
