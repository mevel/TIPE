open Utils.Defs
open Utils.Defs.BinImg

let get_rgb color =
  (color/65536 land 255, color/256 land 255, color land 255)

let read_image path =
  let timer = new Utils.timer in
  let img = Image.read path in
  timer#step ();
  let w = Array.length img.(0)
  and h = Array.length img in
  let px = Array.create_matrix h w false in
  for i = 0 to h-1 do
    for j = 0 to w-1 do
      let (r,g,b) = get_rgb img.(i).(j) in
      px.(i).(j) <- Config.dark_threshold <= r+g+b
    done
  done;
 if Config.timers then
   timer#print ["img_read"; "img_crop"];
 {w; h; px}

let crop_image img =
  let (xmin, xmax) = (ref img.w, ref 0)
  and (ymin, ymax) = (ref img.h, ref 0) in
  for i = 0 to img.h-1 do
    for j = 0 to img.w-1 do
      if not img.px.(i).(j) then begin
        xmin := min !xmin j;
        xmax := max !xmax j;
        ymin := min !ymin i;
        ymax := max !ymax i
      end
    done
  done;
  let w = !xmax - !xmin + 1
  and h = !ymax - !ymin + 1 in
  let px = Array.init h (fun i -> Array.sub img.px.(!ymin+i) !xmin w) in
  {w; h; px}
  
let preproc_image path = crop_image (read_image path)


open Utils.Defs.Rect

let img_sub img rect =
  { BinImg.h = rect.h;
    BinImg.w = rect.w;
    BinImg.px =
     Array.init rect.h (fun i -> Array.sub img.BinImg.px.(rect.y+i) rect.x rect.w)
  }

let connex_areas img =
  (* Fill connex areas. *)
  let px = Array.make_matrix img.BinImg.h img.BinImg.w (-1)
  and partition = new Utils.mergefind in
  for i = 0 to img.BinImg.h-1 do
    for j = 0 to img.BinImg.w-1 do
      if not img.BinImg.px.(i).(j) then begin
        let left = if j > 0 then px.(i).(j-1) else -1
        and top = if i > 0 then px.(i-1).(j) else -1 in
        if left >= 0 && top >= 0 then begin
          partition#merge left top;
          px.(i).(j) <- left
        end else if left >= 0 then
          px.(i).(j) <- left
        else if top >= 0 then
          px.(i).(j) <- top
        else begin
          px.(i).(j) <- partition#nmax;
          partition#add_singleton ()
        end
      end
    done;
  done;
  (* Associate each class to an integer between 0 and n-1. *)
  let n = partition#count in
  let ordinate = Array.create partition#nmax (-1) in
  let count = ref 0 in
  for k = 0 to partition#nmax-1 do
    let k' = partition#find k in
    if ordinate.(k') >= 0 then
      ordinate.(k) <- ordinate.(k')
    else begin
      ordinate.(k') <- !count;
      ordinate.(k) <- !count;
      incr count
    end
  done;
  (* Extract areas. *)
  let rects = Array.init n (fun _ -> {y=max_int; x=max_int; h=1; w=1})
  and areas = Array.init n (fun _ -> BinImg.create img.BinImg.h img.BinImg.w) in
  for i = 0 to img.BinImg.h-1 do
    for j = 0 to img.BinImg.w-1 do
      if px.(i).(j) >= 0 then begin
      let k = ordinate.(px.(i).(j)) in
      if k >= 0 then begin
        areas.(k).BinImg.px.(i).(j) <- false;
        if i < rects.(k).y then begin
          if rects.(k).y <> max_int then
            rects.(k).h <- rects.(k).h + rects.(k).y - i;
          rects.(k).y <- i
        end
        else if i > rects.(k).y + rects.(k).h - 1 then
          rects.(k).h <- i - rects.(k).y + 1;
        if j < rects.(k).x then begin
          if rects.(k).x <> max_int then
            rects.(k).w <- rects.(k).w + rects.(k).x - j;
          rects.(k).x <- j
        end
        else if j > rects.(k).x + rects.(k).w - 1 then
          rects.(k).w <- j - rects.(k).x + 1;
      end
      end
    done
  done;
  Array.to_list
   (Array.mapi (fun k area -> (rects.(k), img_sub area rects.(k))) areas)
