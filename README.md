# TIPE — Reconnaissance de formules mathématiques

Ceci est le code source de mon TIPE (travail personnel d’initiative encadrée). Il s’agit d’un logiciel de reconnaissance de formules mathématiques dans une image.


## Compilation

La compilation nécessite les outils suivants :

- le compilateur OCaml ;
- l’utilitaire `ocamlfind`.

Ce programme nécessite la bibliothèque [`onnt`]. Pour l’installer, décompresser l’archive fournie, se rendre dans le dossier créé et y lancer les commandes :

```sh
make
make install
```

Elle peut être désinstallée ensuite via `make uninstall` ou `ocamlfind remove onnt`.

Pour compiler le programme lui-même, exécuter `make` à la racine du projet.


## Utilisation

La compilation produit deux exécutables, `ocr` et `mathform`. Le premier reconnaît des symboles isolés. Les commandes disponibles sont :

```sh
ocr --train      # Entraîner le système sur le répertoire ‘train’.
ocr --test       # Lancer une série de tests sur le répertoire ‘test’.
ocr img1 img2 …  # Tester le système sur les images fournies.
```

Le second exécutable reconnaît des formules mathématiques. Il s’utilise ainsi :

```sh
mathform img1 img2 …    # Extrait la formule contenue dans chaque image.
```

Les informations affichées sont le graphe initial et, pour chaque étape de transformation, les règles trouvées et le graphe obtenu après application de la règle choisie.

L’affichage d’un graphe contient la liste des nœuds avec leurs numéros, attributs géométriques (taille, position, ligne de référence) et leur étiquette, suivie de la matrice d’adjacence.


[`onnt`]: http://code.google.com/p/ocaml-onnt/