TARGETS = ocr mathform
COMPILER = ocamlfind ocamlc -c -g
LINKER = ocamlfind ocamlc -linkpkg -g
LFLAGS = -package onnt graphics.cma
CFLAGS = -package onnt -I obj/
DEPENDS_OCR = Config Utils Image Preproc Features OCR
DEPENDS_MATHFORM = $(DEPENDS_OCR) Lexer Graph Grammar GrammarRules
OBJ_OCR = $(DEPENDS_OCR:%=obj/%.cmo)
OBJ_MATHFORM = $(DEPENDS_MATHFORM:%=obj/%.cmo)
SRC = $(wildcard src/*.ml*)
DEP = $(SRC:src/%=dep/%.dep)

#.ONESHELL:

.PHONY: all target clean

target: $(TARGETS)

$(TARGETS):
	$(LINKER) $(LFLAGS) $^ -o $@
ocr: $(OBJ_OCR) obj/OCR_main.cmo
mathform: $(OBJ_MATHFORM) obj/Mathform_main.cmo

clean:
	rm -f $(TARGETS)
	rm -f obj/*

all: clean target

obj/%.cmo: src/%.ml
	$(COMPILER) $(CFLAGS) -o $@ $<

obj/%.cmi: src/%.mli
	$(COMPILER) $(CFLAGS) -o $@ $<
#.PRECIOUS: obj/%.cmi    # Prevent the removal of .cmi files.

dep/%.dep: src/%
	ocamldep -I src/ $< |sed 's#\(^\| \)src/#\1obj/#g' > $@

-include $(DEP)
